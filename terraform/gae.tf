resource "google_project_service" "appengine_api" {
  project = "${google_project.project.project_id}"
  service = "appengine.googleapis.com"

  disable_dependent_services = true
}


resource "google_app_engine_application" "app" {
  project     = "${google_project.project.project_id}"
  location_id = "${var.region}"
}
