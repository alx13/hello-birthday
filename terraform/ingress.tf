variable "nginx_ingress_controller_name" {
  default = "controller"
}

resource "helm_release" "nginx_ingress" {
  name  = "nginx-ingress"
  chart = "${path.module}/helm/nginx-ingress"

  set {
    name = "controller.name"
    value = "${var.nginx_ingress_controller_name}"
  }
}

data "kubernetes_service" "nginx_ingress" {
  metadata {
    name = "${helm_release.nginx_ingress.name}-${var.nginx_ingress_controller_name}"
  }
}

output "ingress_external_ip" {
  value = "${data.kubernetes_service.nginx_ingress.load_balancer_ingress.0.ip}"
}

resource "google_dns_record_set" "ingress" {
  name = "${var.demo_app_subdomain}.${google_dns_managed_zone.primary_zone.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = "${google_dns_managed_zone.primary_zone.name}"

  rrdatas = ["${data.kubernetes_service.nginx_ingress.load_balancer_ingress.0.ip}"]
}