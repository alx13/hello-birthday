
resource "google_dns_managed_zone" "primary_zone" {
  name = "${var.dns_zone_name}"
  dns_name = "${var.primary_zone_domain}"
  description = "Root dns zone"
}
