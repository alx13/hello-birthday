resource "google_project_service" "kubernetes_api" {
  project = "${google_project.project.project_id}"
  service = "container.googleapis.com"

  disable_dependent_services = true
}

resource "google_container_cluster" "primary_cluster" {
  name     = "${var.gke_cluster_name}"
  location = "${var.gke_cluster_location}"

  remove_default_node_pool = true
  initial_node_count       = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
  timeouts {
    create = "20m"
    delete = "20m"
  }
}

resource "google_container_node_pool" "primary_nodes" {
  name       = "${var.gke_cluster_name}-nodes"
  location   = "${google_container_cluster.primary_cluster.location}"
  cluster    = "${google_container_cluster.primary_cluster.name}"
  node_count = 2

  management {
    auto_repair = true
  }

  node_config {
    preemptible  = false
    machine_type = "g1-small"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      # FIXME: this permission included just for the test assigment
      # to save time by skipping the creation of Serivce Account
      # with correct rights and passing .json with creds to pod
      "https://www.googleapis.com/auth/datastore",
    ]
  }
  timeouts {
    create = "20m"
    delete = "20m"
  }
}

provider "kubernetes" {
  host     = "${google_container_cluster.primary_cluster.endpoint}"

  cluster_ca_certificate = "${base64decode(google_container_cluster.primary_cluster.master_auth.0.cluster_ca_certificate)}"
  client_certificate     = "${base64decode(google_container_cluster.primary_cluster.master_auth.0.client_certificate)}"
  client_key             = "${base64decode(google_container_cluster.primary_cluster.master_auth.0.client_key)}"
}

output "client_certificate" {
  value = "${google_container_cluster.primary_cluster.master_auth.0.client_certificate}"
  sensitive = true
}

output "client_key" {
  value = "${google_container_cluster.primary_cluster.master_auth.0.client_key}"
  sensitive = true
}

output "cluster_ca_certificate" {
  value = "${google_container_cluster.primary_cluster.master_auth.0.cluster_ca_certificate}"
  sensitive = true
}

output "host" {
  value = "${google_container_cluster.primary_cluster.endpoint}"
  sensitive = true
}
