provider "helm" {
  tiller_image = "gcr.io/kubernetes-helm/tiller:v2.14.1"
  install_tiller = true
  service_account = "tiller"
  kubernetes {
    host = "${google_container_cluster.primary_cluster.endpoint}"


    cluster_ca_certificate = "${base64decode(google_container_cluster.primary_cluster.master_auth.0.cluster_ca_certificate)}"
    client_certificate     = "${base64decode(google_container_cluster.primary_cluster.master_auth.0.client_certificate)}"
    client_key             = "${base64decode(google_container_cluster.primary_cluster.master_auth.0.client_key)}"
  }
}

resource "kubernetes_cluster_role_binding" "helm_crb" {
  metadata {
    name = "tiller"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "tiller"
    namespace = "kube-system"
  }
}

resource "kubernetes_service_account" "tiller_service_account" {
  metadata {
    name = "tiller"
    namespace = "kube-system"
  }
}
