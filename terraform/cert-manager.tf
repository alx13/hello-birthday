resource "kubernetes_namespace" "cert_manager_namespace" {
  metadata {
    name = "cert-manager"
    labels = {
      "certmanager.k8s.io/disable-validation" = true
    }
  }
}

resource "helm_release" "cert_manager_crd" {
  name       = "cert-manager-crd"
  chart      = "${path.module}/helm/cert-manager-crd"
}

resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  chart      = "${path.module}/helm/cert-manager"
  set {
    name = "webhook.enabled"
    value = false
  }
  # we need to install Custom Resource Definition
  # before the helm chart for cert-manager
  depends_on = [
    helm_release.cert_manager_crd,
  ]
}

resource "helm_release" "cert_issuer" {
  name       = "cert-issuer"
  chart      = "${path.module}/helm/cert-issuer"
  set {
    name = "servers.production.email"
    value = "${var.cert_issuer_email}"
  }
  set {
    name = "servers.staging.email"
    value = "${var.cert_issuer_email}"
  }

  # we need to have cert manager installed
  depends_on = [
    helm_release.cert_manager,
  ]
}