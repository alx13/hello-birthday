output "project_id" {
 value = "${google_project.project.project_id}"
}

provider "google" {
 credentials = "${file("${var.google_creds}")}"
 project      = "${var.project_name}"
 region       = "${var.region}"
}

resource "google_project" "project" {
 name            = "${var.project_name}"
 project_id      = "${var.project_name}"
}

resource "google_project_service" "cloudresourcemanager_api" {
  project = "${google_project.project.project_id}"
  service = "cloudresourcemanager.googleapis.com"
  disable_dependent_services = true
}
