## Task
#### Design and code simple "Hello World" application that exposes the following HTTP-based APIs:


```text
Description: Saves/updates the given user's name and date of birth in the database.
Request:     PUT /hello/<username> { "dateOfBirth": "YYYY-MM-DD" }
Response:    204 No Content

Note:
- `<username>` must contains only letters.
- YYYY-MM-DD must be a date before the today date.


Description: Returns hello birthday message for the given user
Request:     GET /hello/<username>
Response:    200 OK
```

Response Examples:

A. If username's birthday is in N days:
```JSON
{ "message": "Hello, <username>! Your birthday is in N day(s)" }
```

B. If username's birthday is today:
```JSON
{ "message": "Hello, <username>! Happy birthday!" }
```

*Note*: Use storage/database of your choice.

#### Produce a system diagram of your solution deployed to either AWS or GCP (it's not requied to support both cloud platforms).

#### Write configuration scripts for building and no-downtime production deployment of application, keeping in mind aspects that an SRE would have to consider.

Implicit requirements:

1. The code produced by you is expected to be of high quality.
2. The solution must have tests, runnalbe locally, and deployable to the cloud.
3. Use common sense.


## Disclaimer
There are lot of things that I could've done if it was a real project.
So here is the incomplete list of things to be done:

- clean-up and test the terraform config (currently it requires some manual steps, and deps chain is a mess)
- create CI/CI pipeline using e.g., GCP Cloud Build
- implement proper logging in the application
- setup error reporting and monitoring
- harden the security in the application (CORS, etc.)
- harden the security in k8s cluster using proper Service Account for the app
- write in-code documentation
- write e2e tests
- implement proper caching in GET requests
- write more bulletproof deploy scripts
- implement rollback strategies

## Intro
Application is written in TypeScript/node.js and tested with current LTS version
of node.js (`10`).

Rules for calculating days till next birthday:

- all time is in UTC and checked against midnight (for the simplicity of test assignment)
- if user's birthday is XXXX-02-29 then calculate days till next leap year
- if user's birthday is in the future then calculate days till actual birth

About the last one: we only check that users birthday is before today with resolver when upserting user.
But if in some case we already have stored user with a birthday in the future, we should great him in some way.

There is an OpenAPI schema located in `app/api.yaml`.
Application utilizes [OpenAPI Backend](https://github.com/anttiviljami/openapi-backend)
npm module to validate requests and responses.

Application uses `Google Cloud Firestore` as storage/database with `username` as object ids.

`username` is limited to 64 characters.

User can't provide invalid birthday (including leap year logic).
We validate that twice — on API level and before interacting with database.

All requests with incorrect parameters will be rejected with `400` HTTP status code.

Current version for deployments is calculated based on short SHA hash of current git commit.
So, it’s possible that there will be conflicts.

Config files with real values for the project are omitted in `.gitignore`.
For each such file there is `.example` one provided with fake values.
In the real project I would consider using [Mozilla sops](https://github.com/mozilla/sops)
to store encrypted config files right in the repository.

To run the app you need to:
- be authentificated in the Google Cloud with `gcloud auth login` and `gcloud auth application-default login`
- have a project in Google Cloud
- have enabled Firestore in Native mode in that project
- populate `app/.env` file with settings (see `app/.env.example`)

To deploy you also need to:
- have `terraform`, `helm`, `kubectl`, `gcloud` tools installed
- be authentificated in the Google Cloud with `gcloud auth login` and `gcloud auth application-default login`
- have a Service Account for terraform in Google Cloud and json with credentials next to that project
- populate all necessary config files (copy from `.example` one and fill)

### Run
```bash
make run-local
```

### Deployment
There is two way to deploy that application and two scripts provided.

####  Google App Engine
```bash
make deploy-app-engine
```

![GAE](diagrams/gae.png)

Script `./script/deploy-app-engine.sh` assumes that we have already transpiled app in the `app/dist` folder.

Deployments steps:

0. Identify current version with git's short SHA
1. Check that current version (see above) is not already deployed on GAE or at least it didn't accept traffic now.
2. Exit if step 1 is failed
3. Deploy current code to GAE without switching traffic to it
4. Check with `curl` that new version is running and can connect to database (using `/_health` route)
5. Migrate all traffic to new version


#### Google Kubernetes Engine
```bash
make deploy-k8s
```
![GKE](diagrams/gke.png)

App is using helm, Deployments resources in k8s and health-checks in pods to pass 'zero-downtime' criteria.

But there is no "rollback" script to remove unstarted release, although it is possible to 

Helm chart is located in the `app/helm` directory.

Deployment steps:

0. Identify current version with git's short SHA
1. Get image name from helm chart
2. Check if current version is not pushed to Google Container Registry
3. Exit if step 2 is failed
4. Build docker container running unit-tests during the build
5. Push docker container to GCR
6. Update helm chart `values.yaml` file with current version
7. Check if any release of the app is already deployed to k8s
8. If check in step 7 is negative - install helm release and exit
9. If check in step 7 is positive — upgrade helm release and exit


### Deployed versions and RPS check
```shell
# K8S
curl https://hello-birthday.alx13.dev/hello/testuser

$ wrk https://hello-birthday.alx13.dev/hello/testuser
Running 10s test @ https://hello-birthday.alx13.dev/hello/testuser
  2 threads and 10 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   225.14ms  139.49ms 852.75ms   81.09%
    Req/Sec    24.30     13.34    67.00     47.62%
  456 requests in 10.06s, 159.87KB read
Requests/sec:     45.32
Transfer/sec:     15.89KB

# GAE
curl https://dev-alx13-demo.appspot.com/hello/testuser

$ wrk https://dev-alx13-demo.appspot.com/hello/testuser
Running 10s test @ https://dev-alx13-demo.appspot.com/hello/testuser
  2 threads and 10 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   132.77ms   65.49ms 487.78ms   78.37%
    Req/Sec    38.46     16.31   100.00     69.36%
  728 requests in 10.08s, 287.22KB read
  Socket errors: connect 0, read 0, write 0, timeout 2
Requests/sec:     72.21
Transfer/sec:     28.49KB
```
