build:
	make -C app build

deploy-app-engine: build
deploy-app-engine:
	./scripts/deploy-app-engine.sh

deploy-k8s: build
deploy-k8s:
	./scripts/deploy-k8s.sh


run-local:
	make -C app start-dev
