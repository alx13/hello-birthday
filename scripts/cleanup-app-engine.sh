#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

source "${DIR}/_common.sh"

JQ_CLEANUP_FILTER="[.[] | select(.traffic_split == 0) | .id] | join(\" \")"
VERSIONS_TO_CLEANUP=$(gcloud app versions list --format json | jq -r "${JQ_CLEANUP_FILTER}")

print "Will delete following inactive versions: ${VERSIONS_TO_CLEANUP}"

gcloud app versions delete \
 ${VERSIONS_TO_CLEANUP} \
 --quiet

 print "Done"