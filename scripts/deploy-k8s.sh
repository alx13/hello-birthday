#!/bin/bash

RELEASE_NAME="hello-birthday"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${DIR}/_common.sh"

cd app

CURRENT_VERSION=$(git log -n 1 --format=%h)
# CURRENT_VERSION="latest"
IMAGE_NAME=$(
  cat ./helm/values.yaml \
  | grep 'repository:' \
  | sed 's/repository://' \
  | sed -e 's/[[:space:]]*//'
)

EXISTING_IMAGES=$(
  gcloud container images list-tags \
    ${IMAGE_NAME} \
    --filter="tags=${CURRENT_VERSION}" \
    --format=json
)

if [ "${FORCE:=false}" != "true" ] && [ "${EXISTING_IMAGES}" != "[]" ]
then
  print "Docker image \"${IMAGE_NAME}:${CURRENT_VERSION}\" already exist. Exiting."
  exit 1
fi

print "building ${IMAGE_NAME}"

docker build \
  -t "${IMAGE_NAME}:${CURRENT_VERSION}" \
  .

docker push \
  "${IMAGE_NAME}:${CURRENT_VERSION}"

sed -i.bak "s/^\([[:space:]].*\)tag: .*$/\1tag: ${CURRENT_VERSION}/" helm/values.yaml

IS_DEPLOYED=$(helm list "${RELEASE_NAME}")

if [ -z "${IS_DEPLOYED}" ]
then
  print "Installing ${RELEASE_NAME} with ${CURRENT_VERSION}"
  helm install \
    --name "${RELEASE_NAME}" \
    helm
  print "Done"
  exit 0
fi

print "Upgrading ${RELEASE_NAME} to ${CURRENT_VERSION}"
helm upgrade \
  "${RELEASE_NAME}" \
  helm
print "Done"
exit 0
