#!/bin/bash

set -o errexit
set -o noglob
set -o nounset
set -o pipefail
# set -o xtrace

function print () {
  echo "====================="
  echo "$1"
  echo "====================="
}