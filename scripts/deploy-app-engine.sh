#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

source "${DIR}/_common.sh"

cd app

CURRENT_VERSION=$(git log -n 1 --format=%h)
JQ_CURRENT_VERSION_FILTER="[.[] | select(.id == \"${CURRENT_VERSION}\")][0] | .traffic_split? | . > 0"
IS_CURRENT_VERSION_DEPLOYED=$(gcloud app versions list --format json | jq "${JQ_CURRENT_VERSION_FILTER}")
DEPLOYMENT_INFO_FILE="./.ops/deployment.json"
JQ_FILTER_GET_DEPLOYMENT_URL=".versions[0] | \"https://\" + .id + \"-dot-\" + .project + \".appspot.com\""

if [ "${IS_CURRENT_VERSION_DEPLOYED}" = "true" ]
then
  print "Current version ${CURRENT_VERSION} is deployed and accepting traffic! Exiting."
  exit 1
fi

mkdir -p  "$(dirname "${DEPLOYMENT_INFO_FILE}")"

gcloud app deploy \
  app.yaml \
  --no-promote \
  --version="${CURRENT_VERSION}" \
  --quiet \
  --format="json" \
| tee ${DEPLOYMENT_INFO_FILE} | jq

DEPLOYED_URL=$(jq -r "${JQ_FILTER_GET_DEPLOYMENT_URL}" ${DEPLOYMENT_INFO_FILE})
# we can use things like https://github.com/aelsabbahy/goss here
CURL_RESULT=$(curl -L "${DEPLOYED_URL}/_health")

if [ "${CURL_RESULT}" != "OK" ]
then
  print "Current version failed health check"
  print "Got following result ${CURL_RESULT}"

  gcloud app versions delete \
    ${CURRENT_VERSION} \
    --quiet
  exit 1
fi

print "Health check successfull. Redirecting traffic"

gcloud app versions migrate \
  ${CURRENT_VERSION} \
  --quiet

print "Done"