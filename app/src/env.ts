export function parseEnv(env: NodeJS.ProcessEnv) {
  function ar(key: string) {
    const value = env[key];
    if (!value) {
      throw new Error(`variable "${key}" is unset in Env`);
    }

    return value;
  }

  return {
    port: parseInt(env['PORT'] || env['DEMO__INTERNAL_PORT'] || '8080', 10),
    gcp: {
      projectId: ar('GOOGLE_CLOUD_PROJECT'),
      firestoreCollection: ar('DEMO__FIRESTORE_COLLECTION'),
    },
  };
}

export type Env = ReturnType<typeof parseEnv>;
