import { StatusCodeError, STATUS_CODE } from './interfaces';

export function checkUsername(username: string) {
  let ok = true;

  // tslint:disable-next-line: strict-type-predicates
  if (typeof username !== 'string') {
    ok = ok && false;
  }

  if (!/^[a-zA-Z]{1,64}$/.test(username)) {
    ok = ok && false;
  }

  if (ok) {
    return;
  }

  throw new StatusCodeError(
    `Wrong username "${username}" format. Should be a string with 1 to 64 letters a-zA-Z`,
    STATUS_CODE.BAD_REQUEST
  );
}

