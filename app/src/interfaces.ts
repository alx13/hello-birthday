export type Maybe<T> = T | null | undefined;

export type MaybePromise<T> = Promise<T> | T;

export const enum STATUS_CODE {
  OK = 200,
  NO_CONTENT = 204,
  BAD_REQUEST = 400,
  NOT_FOUND = 404,
  METHOD_NOT_ALLOWED = 405,
  INTERNAL_SERVER_ERROR = 500,
  NOT_IMPLEMENTED = 501,
}

export class StatusCodeError extends Error {
  public constructor(
    message: string,
    code: STATUS_CODE = STATUS_CODE.INTERNAL_SERVER_ERROR,
    errors?: Maybe<object[]>
  ) {
    super(message);

    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
    this.code = code || STATUS_CODE.INTERNAL_SERVER_ERROR;
    this.errors = errors;
  }

  public code: number;
  public errors: Maybe<object[]>;
}

export interface User {
  username: string;
  dateOfBirth: string;
}

export interface Store {
  upsertUser(user: User): MaybePromise<void>;
  getUserByUsername(username: string): MaybePromise<Maybe<User>>;
  healthCheck(): MaybePromise<void>;
}

export interface ResolverResponse {
  code: STATUS_CODE;
  content?: object;
}

export interface Resolver {
  upsertUser(user: User): MaybePromise<void>;
  greetUserByUsername(username: string): MaybePromise<string>;
}
