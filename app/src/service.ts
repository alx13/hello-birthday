import {
  Router,
  Request,
  Response,
} from 'express';
import {
  STATUS_CODE,
  Store,
} from './interfaces';

function healthCheck(store: Store) {
  return (_req: Request, res: Response) => {
    Promise.resolve()
      .then(async () => {
        await store.healthCheck();
        res.sendStatus(STATUS_CODE.OK);
      })
      .catch((err) => {
        console.error(err);
        res.sendStatus(STATUS_CODE.INTERNAL_SERVER_ERROR);
      });
  };
}

export interface InitServiceRouterParams {
  store: Store;
}
export async function initServiceRouter({
  store,
}: InitServiceRouterParams) {
  const router = Router({
    strict: true,
  });

  router.get('/_ah/warmup', healthCheck(store));
  router.get('/_health', healthCheck(store));

  return router;
}
