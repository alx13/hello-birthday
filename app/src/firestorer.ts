import assert from 'assert';
import { Store, User } from './interfaces';
import { Firestore, CollectionReference } from '@google-cloud/firestore';
import { formatDateOfBirth, parseDateToMidnight } from './birthday';
import { checkUsername } from './username';

export interface FirestorerOptions {
  projectId: string;
  collectionName: string;
}

export class Firestorer implements Store {
  public constructor({
    projectId,
    collectionName,
  }: FirestorerOptions) {
    this.firestore = new Firestore({
      projectId,
    });

    this.collection = this.firestore.collection(collectionName);
  }

  private readonly firestore: Firestore;
  private readonly collection: CollectionReference;

  public async upsertUser({ username, dateOfBirth }: User) {
    checkUsername(username);
    // checks if date is valid
    await this.collection.doc(username).set({
      dateOfBirth: formatDateOfBirth(parseDateToMidnight(dateOfBirth)),
    });
  }

  public async getUserByUsername(username: string) {
    checkUsername(username);
    const doc = await this.collection.doc(username).get();
    if (!doc.exists) {
      return null;
    }
    const { dateOfBirth } = doc.data() as { dateOfBirth: string };

    return {
      username,
      dateOfBirth,
    };
  }

  public async healthCheck() {
    const result = await this.collection.limit(1).get();

    assert.ok(result, 'Failed to connect to firestore');
  }
}
