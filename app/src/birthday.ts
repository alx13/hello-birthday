import dayjs, { Dayjs } from 'dayjs';
import utc from 'dayjs/plugin/utc';
dayjs.extend(utc);

import {
  StatusCodeError,
  STATUS_CODE,
} from './interfaces';

export function isLeapYear(year: number) {
  if (year % 4 !== 0) {
    return false;
  }

  if (year % 100 !== 0) {
    return true;
  }

  if (year % 400 !== 0) {
    return false;
  }

  return true;
}

// NB: source date string supposed to be in UTC
export function parseDateToMidnight(date: string) {
  const parsedDate = dayjs.utc(`${date}T00:00:00.000Z`);
  if (date !== parsedDate.format('YYYY-MM-DD')) {
    throw new Error(`Imposible date "${date}"`);
  }

  return parsedDate;
}

export function formatDateOfBirth(date: Dayjs) {
  return date.utc().format('YYYY-MM-DD');
}

export function assertIsPast(now: Dayjs, date: Dayjs) {
  if (!date.isBefore(now)) {
    throw new StatusCodeError(
      `Date should be in the past. Provided date ${formatDateOfBirth(date)}, today is ${formatDateOfBirth(now)}`,
      STATUS_CODE.BAD_REQUEST
    );
  }

  return date;
}

// TODO: optimize
export function findNearest0229Date(now: Dayjs) {
  const nd = parseDateToMidnight(now.format('YYYY-MM-DD'));
  if (now.format('MM-DD') === '02-29') {
    return parseDateToMidnight(now.format('YYYY-MM-DD'));
  }

  const currentYear = now.get('year');
  const currentYearPlus9 = currentYear + 9;

  // 1896 - leap, 1900 - common, 1904 - leap
  for (let year = currentYear; year <  currentYearPlus9; year += 1) {
    if (!isLeapYear(year)) {
      continue;
    }
    const year0229 = parseDateToMidnight(`${year}-02-29`);
    if (year0229.isAfter(nd)) {
      return year0229;
    }
  }
  throw new Error(`something went wrong finding next leap year from ${now.toString()}`);
}

export function findNextBirthdayDate(now: Dayjs, dob: Dayjs) {
  if (dob.isAfter(now)) {
    return dob;
  }

  if (dob.format('MM-DD') === '02-29') {
    return findNearest0229Date(now);
  }

  const birthdayDateThisYear = dob.set('year', now.year());

  return birthdayDateThisYear.isBefore(now)
    ? birthdayDateThisYear.set('year', now.year() + 1)
    : birthdayDateThisYear;
}


export function getDaysToNextBirthday(today: string, dateOfBirth: string) {
  const now = parseDateToMidnight(today);
  const dob = parseDateToMidnight(dateOfBirth);
  const nextBirthdayDate = findNextBirthdayDate(now, dob);

  return nextBirthdayDate.diff(now, 'day');
}
