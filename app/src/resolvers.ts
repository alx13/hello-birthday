import dayjs from 'dayjs';

import {
  getDaysToNextBirthday,
  formatDateOfBirth,
  assertIsPast,
  parseDateToMidnight,
} from './birthday';
import {
  Store,
  User,
  Resolver,
  STATUS_CODE,
  StatusCodeError,
} from './interfaces';

export interface HelloResolverOptions {
  store: Store;
}
export class HelloResolver implements Resolver {
  public constructor({ store }: HelloResolverOptions) {
    this.store = store;
  }
  private readonly store: Store;

  public async upsertUser(user: User) {
    assertIsPast(
      parseDateToMidnight(formatDateOfBirth(dayjs())),
      parseDateToMidnight(user.dateOfBirth)
    );

    await this.store.upsertUser(user);
  }

  public async greetUserByUsername(username: string) {
    const user = await this.store
      .getUserByUsername(username);

    if (!user) {
      throw new StatusCodeError(
        'userNotFound',
        STATUS_CODE.NOT_FOUND
      );
    }

    const daysToNearestBirthday = getDaysToNextBirthday(
      formatDateOfBirth(dayjs()),
      user.dateOfBirth
    );
    const birthdayInfo = daysToNearestBirthday > 0
      ? `Your birthday is in ${daysToNearestBirthday} day(s)`
      : 'Happy birthday!';

    return `Hello, ${username}! ${birthdayInfo}`;
  }
}
