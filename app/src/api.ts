import path from 'path';
import express from 'express';
import OpenAPIBackend, {
  Handler,
  Context,
  ParsedRequest,
  ValidationResult,
} from 'openapi-backend';
import {
  StatusCodeError,
  STATUS_CODE,
  Resolver,
} from './interfaces';

interface HandlerResponse {
  code: number;
  content?: object;
}

function wrapHandler(
  { silentApiErrors = false }: { silentApiErrors?: boolean },
  handler: (
    ctx: Context,
    req: express.Request,
    res: express.Response
  ) => Promise<HandlerResponse>
) {
  return (async (ctx, req, res) => {
    try {
      if (!ctx) {
        throw new StatusCodeError(
          'No context',
          STATUS_CODE.INTERNAL_SERVER_ERROR
        );
      }

      const result = await handler(ctx, req, res);
      const { code, content } = result;

      const { api, operation } = ctx;
      if (!operation) {
        throw new StatusCodeError(
          'Method Not Allowed',
          STATUS_CODE.METHOD_NOT_ALLOWED
        );
      }

      const {
        valid: responseIsValid,
        errors: responseValidationErrors,
      } = api.validateResponse(
        content,
        operation,
        code
      );

      if (!responseIsValid) {
        throw new StatusCodeError(
          'Response validation error',
          STATUS_CODE.INTERNAL_SERVER_ERROR,
          responseValidationErrors
        );
      }

      if (content === undefined) {
        res
          .sendStatus(code);

        return;
      }

      res
        .status(code)
        .json(content);
    } catch (e) {
      if (!silentApiErrors) {
        console.error(e);
      }

      if (e instanceof StatusCodeError) {
        res
          .status(e.code)
          .json({
            code: e.code,
            message: e.message,
            errors: e.errors,
          });

        return;
      }

      res
        .status(STATUS_CODE.INTERNAL_SERVER_ERROR)
        .json({
          code: STATUS_CODE.INTERNAL_SERVER_ERROR,
          message: e.message,
        });
    }
  }) as Handler;
}

export interface InitApiOptions {
  options: {
    silentApiErrors?: boolean;
  };
  resolver: Resolver;
}

export async function initApi({ options, resolver }: InitApiOptions) {
  const api = new OpenAPIBackend({
    strict: true,
    validate: true,
    definition: path.join(__dirname, '../api.yaml'),
    ajvOpts: {
      format: 'full',
    },
    handlers: {
      upsertUserByName: wrapHandler(options, async (ctx) => {
        const { request } = ctx as { request: ParsedRequest };
        const { username } = request.params as { username: string };
        const { dateOfBirth } = request.body as { dateOfBirth: string };

        await resolver.upsertUser({ username, dateOfBirth });

        return {
          code: STATUS_CODE.NO_CONTENT,
        };
      }),
      greetUserByName: wrapHandler(options, async (ctx) => {
        const { request } = ctx as { request: ParsedRequest };
        const { username } = request.params as { username: string };

        const message = await resolver.greetUserByUsername(username);

        return {
          code: STATUS_CODE.OK,
          content: { message },
        };
      }),

      validationFail: wrapHandler(options, async (ctx) => {
        const { validation } = ctx as { validation: ValidationResult };

        throw new StatusCodeError(
          'Input validation failed',
          STATUS_CODE.BAD_REQUEST,
          validation.errors
        );
      }),

      notFound: wrapHandler(options, async () => {
        throw new StatusCodeError(
          'Not found',
          STATUS_CODE.NOT_FOUND
        );
      }),

      notImplemented: wrapHandler(options, async () => {
        throw new StatusCodeError(
          'Not implemented',
          STATUS_CODE.NOT_IMPLEMENTED
        );
      }),
    },
  });

  await api.init();

  return api;
}
