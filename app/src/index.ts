import express from 'express';
import {
  Request as OpenApiRequest,
} from 'openapi-backend';
import { parseEnv, Env } from './env';
import { initApi } from './api';
import { initServiceRouter } from './service';
import { HelloResolver } from './resolvers';
import { Firestorer } from './firestorer';

export async function initApp(env: Env) {
  const firestorer = new Firestorer({
    projectId: env.gcp.projectId,
    collectionName: env.gcp.firestoreCollection,
  });

  const resolver = new HelloResolver({
    store: firestorer,
  });

  const serviceRouter = await initServiceRouter({
    store: firestorer,
  });

  const api = await initApi({
    resolver,
    // TODO: move to env
    options: {
      silentApiErrors: false,
    },
  });

  const app = express();
  app.set('trust proxy', true);

  app.use(express.json());
  app.use('/', serviceRouter);
  app.use((req, res) => api.handleRequest(req as OpenApiRequest, req, res));

  return app;
}

if (require.main === module) {
  const env = parseEnv(process.env);
  initApp(env)
  .then((app) => {
    app.listen(env.port);

    console.log(`server is listening on port ${env.port}`);
  })
  .catch((err) => {
    console.error(err);

    process.exit(1);
  });
}
