import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
dayjs.extend(utc);

import { assert } from 'chai';
import {
  isLeapYear,
  parseDateToMidnight as pdtm,
  getDaysToNextBirthday,
  findNearest0229Date,
} from '../src/birthday';


describe('birthday', () => {
  describe('isLeapYear', () => {
    const tcs = [
      { src: 1896, exp: true },
      { src: 1897, exp: false },
      { src: 1900, exp: false },
      { src: 1901, exp: false },
      { src: 1904, exp: true },

      { src: 1996, exp: true },
      { src: 1997, exp: false },
      { src: 2000, exp: true },
      { src: 2001, exp: false },
      { src: 2004, exp: true },
    ];

    tcs.forEach((tc) => {
      it(`${tc.src} -> ${tc.exp}`, () => {
        assert.equal(isLeapYear(tc.src), tc.exp);
      });
    });
  });

  describe('parseDateMidnight', () => {
    const tcs = [
      { src: '2019-07-01', throws: false },
      { src: '2030-07-01', throws: false },
      { src: '2019-02-29', throws: true },
      { src: '2016-02-29', throws: false },
    ];

    tcs.forEach((tc) => {
      it(`parseDateToMidnight(${tc.src}) ${tc.throws ? 'throws' : 'ok'}`, () => {
        if (tc.throws) {
          assert.throws(() => { pdtm(tc.src); });

          return;
        }
        const parsed = pdtm(tc.src);

        assert.deepEqual(parsed.format('YYYY-MM-DD'), tc.src);
        assert.equal(parsed.utc().format('HH:mm:ss:SSSZZ'), '00:00:00:000+0000');
      });
    });
  });
  describe('findNearest0229Date', () => {
    const tcs = [
      { src: '1896-02-29', exp: '1896-02-29' },
      { src: '1896-03-01', exp: '1904-02-29' },
      { src: '1900-02-28', exp: '1904-02-29' },
      { src: '2016-01-01', exp: '2016-02-29' },
      { src: '2019-01-01', exp: '2020-02-29' },
    ];

    tcs.forEach((tc) => {
      it(`${tc.src} -> ${tc.exp}`, () => {
        assert.equal(
          findNearest0229Date(pdtm(tc.src)).format('YYYY-MM-DD'),
          tc.exp
        );
      });
    });
  });
  // TODO: maybe write some more tests
  // tslint:disable-next-line: no-empty
  describe.skip('findNextBirthdayDay', () => {
  });

  describe('getDaysToNextBirthday', () => {
    const tcs = [
      { src: { today: '2019-07-01', dob: '2019-07-01' }, exp: 0 },
      { src: { today: '2019-06-30', dob: '1980-07-01' }, exp: 1 },
      { src: { today: '2016-02-29', dob: '2012-02-29' }, exp: 0 },
      {
        src: { today: '2017-01-01', dob: '2019-01-01' },
        exp: pdtm('2019-01-01').diff(pdtm('2017-01-01'), 'day'),
      },
      {
        src: { today: '2017-01-01', dob: '2019-01-01' },
        exp: pdtm('2019-01-01').diff(pdtm('2017-01-01'), 'day'),
      },

      // leap year logic
      { src: { today: '2016-02-28', dob: '2012-02-29' }, exp: 1 },
      {
        src: { today: '2017-02-28', dob: '2012-02-29' },
        exp: pdtm('2020-02-29').diff(pdtm('2017-02-28'), 'day'),
      },
      {
        src: { today: '1896-03-01', dob: '1896-02-29' },
        exp: pdtm('1904-02-29').diff(pdtm('1896-03-01'), 'day'),
      },

      // future
      {
        src: { today: '2019-07-01', dob: '2064-02-03' },
        exp: pdtm('2064-02-03').diff(pdtm('2019-07-01'), 'day'),
      },
    ];

    tcs.forEach((tc) => {
      it(`(now = ${tc.src.today}, dob = ${tc.src.dob}) => ${tc.exp}`, () => {
        assert.equal(getDaysToNextBirthday(tc.src.today, tc.src.dob), tc.exp);
      });
    });
  });
});
