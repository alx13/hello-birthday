// tslint:disable: no-unbound-method
import { Server } from 'http';
import { AddressInfo } from 'net';

import express from 'express';
import fetch from 'node-fetch';
import { Request as OpenApiRequest } from 'openapi-backend';
import { assert } from 'chai';
import { stub, SinonStub } from 'sinon';

import {
  STATUS_CODE,
  Resolver,
  StatusCodeError,
} from '../src/interfaces';
import { initApi } from '../src/api';


describe('api spec', () => {
  let server: Server;
  const resolver: Resolver = {
    greetUserByUsername: stub()
      .callsFake(async (username: string) => {
        if (username === 'missing') {
          throw new StatusCodeError(
            'User not found',
            STATUS_CODE.NOT_FOUND
          );
        }

        return username;
      }),
    upsertUser: stub().resolves(),
  };

  before(async () => {
    const api = await initApi({
      resolver,
      options: {
        silentApiErrors: true,
      },
    });

    const app = express();
    app.use(express.json());
    app.use((req, res) => api.handleRequest(req as OpenApiRequest, req, res));

    return new Promise((resolve, reject) => {
      server = app.listen((err, res) => {
        if (err) {
          return reject(err);
        }

        return resolve(res);
      });
    });
  });
  describe('put user', async () => {
    const resolverFn = resolver.upsertUser as SinonStub;
    const tcs = [
      {
        src: { username: 'test', body: { dateOfBirth: '2018-01-01' } },
        exp: { code: STATUS_CODE.NO_CONTENT },
      },
      {
        src: { username: 'abcdefgh'.repeat(8), body: { dateOfBirth: '2018-01-01' } },
        exp: { code: STATUS_CODE.NO_CONTENT },
      },
      // invalid username
      {
        src: { username: '', body: { dateOfBirth: '2018-01-01' } },
        exp: { code: STATUS_CODE.NOT_FOUND },
      },
      {
        src: { username: '1', body: { dateOfBirth: '2018-01-01' } },
        exp: { code: STATUS_CODE.BAD_REQUEST },
      },
      {
        src: { username: 'abcdefgh'.repeat(9), body: { dateOfBirth: '2018-01-01' } },
        exp: { code: STATUS_CODE.BAD_REQUEST },
      },
      // invalid body
      {
        src: { username: 'test', body: {} },
        exp: { code: STATUS_CODE.BAD_REQUEST },
      },
      {
        src: { username: 'test' },
        exp: { code: STATUS_CODE.BAD_REQUEST },
      },
      {
        src: { username: 'test', body: { dateOfBirth: '2018-01-01', unexpectedProperty: true } },
        exp: { code: STATUS_CODE.BAD_REQUEST },
      },
      // invalid date of birth
      {
        src: { username: 'test', body: { dateOfBirth: '2018-14-01' } },
        exp: { code: STATUS_CODE.BAD_REQUEST },
      },
      {
        src: { username: 'test', body: { dateOfBirth: '2018-02-29' } },
        exp: { code: STATUS_CODE.BAD_REQUEST },
      },
      {
        src: { username: 'test', body: { dateOfBirth: '20.01.2018' } },
        exp: { code: STATUS_CODE.BAD_REQUEST },
      },
    ];

    tcs.forEach(({ src, exp }) => {
      it(`${JSON.stringify(src)} -> ${JSON.stringify(exp)}`, async () => {
        resolverFn.resetHistory();
        const { port } = server.address() as AddressInfo;

        const result = await fetch(
          `http://localhost:${port}/hello/${src.username}`,
          {
            method: 'PUT',
            body: JSON.stringify(src.body),
            headers: { 'Content-Type': 'application/json' },
          }
        );

        assert.equal(result.status, exp.code);

        if (exp.code === STATUS_CODE.NO_CONTENT) {
          assert.isOk(resolverFn.calledOnce);
          assert.isEmpty(await result.text());

          return;
        }
        assert.isNotOk(resolverFn.called);
      });
    });
  });
  describe('get user', async () => {
    const resolverFn = resolver.greetUserByUsername as SinonStub;
    const tcs = [
      {
         src: { username: 'test' },
         exp: { code: STATUS_CODE.OK },
      },
      {
         src: { username: 'abcdefgh'.repeat(8) },
         exp: { code: STATUS_CODE.OK },
      },
      // missing user
      {
         src: { username: 'missing' },
         exp: { code: STATUS_CODE.NOT_FOUND },
      },
      // invalid username
      {
         src: { username: '' },
         exp: { code: STATUS_CODE.NOT_FOUND },
      },
      {
         src: { username: '1' },
         exp: { code: STATUS_CODE.BAD_REQUEST },
      },
      {
         src: { username: 'abcdefgh'.repeat(9) },
         exp: { code: STATUS_CODE.BAD_REQUEST },
      },
    ];

    tcs.forEach(({ src, exp }) => {
      it(`${JSON.stringify(src)} -> ${JSON.stringify(exp)}`, async () => {
        resolverFn.resetHistory();
        const { port } = server.address() as AddressInfo;

        const result = await fetch(
          `http://localhost:${port}/hello/${src.username}`,
          {
            method: 'GET',
          }
        );

        assert.equal(result.status, exp.code, 'status code missmatch');
        assert.equal(result.headers.get('Content-Type'), 'application/json; charset=utf-8');

        if (exp.code === STATUS_CODE.BAD_REQUEST) {
          assert.isNotOk(
            resolverFn.called,
            'resolver should not be called with malformed request'
          );

          return;
        }

        // special case for calling 'get /hello/'
        if (src.username === '' && exp.code === STATUS_CODE.NOT_FOUND) {
          return;
        }

        assert.isOk(
          resolverFn.calledOnce,
          'resolver should be called once'
        );

        if (exp.code !== STATUS_CODE.OK) {
          return;
        }

        assert.deepEqual(
          await result.json(),
          {
            message: src.username,
          }
        );
      });
    });
  });

  after((done) => {
    server.close(done);
  });
});
