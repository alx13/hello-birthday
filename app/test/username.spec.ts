import { assert } from 'chai';
import { checkUsername } from '../src/username';

describe('username', () => {
  describe('checkUsername', () => {
    const tcs = [
      // null
      { src: null as unknown as string, throws: true },
      // undefined
      { src: undefined as unknown as string, throws: true },
      // number
      { src: 0xF as unknown as string, throws: true },
      // empty
      { src: '', throws: true },
      // ok
      { src: 'a', throws: false },
      // non-letter symbol
      { src: '1', throws: true },
      // non-letter symbol
      { src: '1a', throws: true },
      { src: 'a1', throws: true },
      // length === 64
      { src: 'abcdefgh'.repeat(8), throws: false },
      // length > 64
      { src: `${'abcdefgh'.repeat(8)}a`, throws: true },
    ];

    tcs.forEach((tc) => {
      it(`checkUsername(${tc.src}) ${tc.throws ? 'throws' : 'ok'}`, () => {
        const assertion = tc.throws
          ? assert.throws.bind(null)
          : assert.doesNotThrow.bind(null);

        assertion(() => checkUsername(tc.src));
      });
    });
  });
});
