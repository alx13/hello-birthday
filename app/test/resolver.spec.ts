// tslint:disable: no-unbound-method
// tslint:disable: await-promise
import dayjs, { Dayjs } from 'dayjs';
import { stub, SinonStub } from 'sinon';
import chai, { assert } from 'chai';
import chaiAsPromised from 'chai-as-promised';
chai.use(chaiAsPromised);

import { Store } from '../src/interfaces';
import { HelloResolver } from '../src/resolvers';

describe('resolver', async () => {
  function formatDate(date: Dayjs) {
    return date.format('YYYY-MM-DD');
  }

  function expectedGreeting(username: string, daysTillBirthday: number) {
    if (daysTillBirthday === 0) {
      return `Hello, ${username}! Happy birthday!`;
    }

    return `Hello, ${username}! Your birthday is in ${daysTillBirthday} day(s)`;
  }

  const USERS = [
    { username: 'today', dtb: 0 },
    { username: 'tomorrow', dtb: 1 },
    { username: 'inthirtyDays', dtb: 1 },
  ];

  const store: Store = {
    upsertUser: stub().resolves(),
    getUserByUsername: stub().callsFake((username: string) => {
      const user = USERS.find((u) => u.username === username);
      if (!user) {
        throw Error(`Missing user "${username}". Typo?`);
      }

      return {
        username,
        dateOfBirth: formatDate(
          dayjs()
            .utc()
            .set(
              'year',
              dayjs().get('year')
            )
            .add(
              user.dtb,
              'day'
            )
        ),
      };
    }),
    healthCheck: stub().resolves(),
  };

  let resolver: HelloResolver;

  before(() => {
    resolver = new HelloResolver({ store });
  });

  describe('greetUserByUsername', () => {
    const storeFn = store.getUserByUsername as SinonStub;
    USERS.forEach((src) => {
      const greeting = expectedGreeting(src.username, src.dtb);
      it(`${src.dtb} => ${greeting}`, async () => {
        storeFn.resetHistory();
        assert.equal(
          await resolver.greetUserByUsername(src.username),
          greeting
        );
        assert.isOk(storeFn.called);
      });
    });
  });
  describe('upsertUser', () => {
    const now = dayjs().utc();
    const tcs = [
      { src: '2000-01-01', throws: false },
      { src: '2001-02-29', throws: true },
      { src: formatDate(now.subtract(1, 'day')), throws: false },
      { src: formatDate(now), throws: true },
      { src: formatDate(now.add(1, 'day')), throws: true },
    ];
    const storeFn = store.upsertUser as SinonStub;

    tcs.forEach((tc) => {
      it(`${tc.src} -> ${tc.throws ? 'throws' : 'ok'}`, async () => {
        storeFn.resetHistory();

        const promise = resolver.upsertUser({
          username: 'testuser',
          dateOfBirth: tc.src,
        });

        if (tc.throws) {
          await assert.isRejected(promise);
          assert.isOk(storeFn.notCalled);

          return;
        }

        await assert.eventually.isUndefined(promise);
        assert.isOk(storeFn.calledOnce);
      });
    });
  });
});
