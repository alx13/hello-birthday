"use strict";
// tslint:disable: max-file-line-count
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
function resolve(mod, root) {
    return path.join(path.dirname(require.resolve(mod)), root);
}
module.exports = {
    linterOptions: {
        exclude: [
            'node_modules/**/*',
            'dist/**/*',
        ],
    },
    rulesDirectory: [
        resolve('tslint-consistent-codestyle', './'),
        resolve('tslint-eslint-rules', 'dist/rules'),
        resolve('tslint-microsoft-contrib', './'),
    ],
    rules: {
        // TSLint internal
        'adjacent-overload-signatures': true,
        'ban-types': false,
        'member-access': [
            true,
            // 'no-public',
            'check-accessor',
            'check-parameter-property',
        ],
        'member-ordering': [
            false,
            {
                order: [
                    'public-static-field',
                    'public-static-method',
                    'protected-static-field',
                    'protected-static-method',
                    'private-static-field',
                    'private-static-method',
                    'public-instance-field',
                    'protected-instance-field',
                    'private-instance-field',
                    'public-constructor',
                    'protected-constructor',
                    'private-constructor',
                    'public-instance-method',
                    'protected-instance-method',
                    'private-instance-method',
                ],
            },
        ],
        'no-any': true,
        'no-empty-interface': true,
        'no-import-side-effect': [
            true,
            {
                'ignore-module': '(\\.html|\\.css)$',
            },
        ],
        'no-inferrable-types': [
            false,
            'ignore-params',
            'ignore-properties',
        ],
        'no-internal-module': true,
        'no-magic-numbers': [
            false,
        ],
        'no-namespace': [
            true,
            'allow-declarations',
        ],
        'no-non-null-assertion': true,
        'no-parameter-reassignment': true,
        'no-reference': true,
        'no-unnecessary-type-assertion': true,
        'no-var-requires': true,
        'only-arrow-functions': [
            true,
            'allow-declarations',
            'allow-named-functions',
        ],
        'prefer-for-of': true,
        'promise-function-async': true,
        'typedef': [
            false,
            // 'call-signature',
            // 'arrow-call-signature',
            'parameter',
            // 'arrow-parameter',
            'property-declaration',
            // 'variable-declaration',
            'member-variable-declaration',
        ],
        'typedef-whitespace': [
            true,
            {
                'call-signature': 'nospace',
                'index-signature': 'nospace',
                'parameter': 'nospace',
                'property-declaration': 'nospace',
                'variable-declaration': 'nospace',
            },
            {
                'call-signature': 'onespace',
                'index-signature': 'onespace',
                'parameter': 'onespace',
                'property-declaration': 'onespace',
                'variable-declaration': 'onespace',
            },
        ],
        'unified-signatures': true,
        'await-promise': [true, 'Bluebird'],
        'ban-comma-operator': true,
        'ban': [
            false,
        ],
        'curly': [
            true,
        ],
        'forin': true,
        'import-blacklist': [
            false,
        ],
        'label-position': true,
        'no-arg': true,
        'no-bitwise': true,
        'no-conditional-assignment': true,
        'no-console': [
            true,
        ],
        'no-construct': true,
        'no-debugger': true,
        'no-duplicate-super': true,
        'no-duplicate-switch-case': true,
        'no-duplicate-variable': [
            true,
            'check-parameters',
        ],
        'no-dynamic-delete': true,
        'no-empty': [
            true,
        ],
        'no-eval': true,
        'no-floating-promises': true,
        'no-for-in-array': true,
        'no-implicit-dependencies': [
            true,
        ],
        'no-inferred-empty-object-type': true,
        'no-invalid-template-strings': true,
        'no-invalid-this': [
            true,
            'check-function-in-method',
        ],
        'no-misused-new': true,
        'no-null-keyword': false,
        'no-object-literal-type-assertion': true,
        'no-return-await': true,
        'no-shadowed-variable': [
            true,
            {
                class: true,
                enum: true,
                function: true,
                interface: true,
                namespace: true,
                typeAlias: true,
                typeParameter: true,
            },
        ],
        'no-sparse-arrays': true,
        'no-string-literal': false,
        'no-string-throw': true,
        'no-submodule-imports': [
            true,
        ],
        'no-switch-case-fall-through': true,
        'no-this-assignment': [
            true,
            {
                // 'allowed-names': ['^self$'],
                'allow-destructuring': true,
            },
        ],
        'no-unbound-method': [
            true,
        ],
        'no-unnecessary-class': [
            true,
        ],
        // FIXME waiting for https://github.com/palantir/tslint/issues/3080
        'no-unsafe-any': false,
        'no-unsafe-finally': true,
        'no-unused-expression': [
            true,
            'allow-fast-null-checks',
        ],
        'no-var-keyword': true,
        'no-void-expression': [
            'ignore-arrow-function-shorthand',
        ],
        'prefer-conditional-expression': [
            true,
            'check-else-if',
        ],
        'prefer-object-spread': true,
        'radix': true,
        'restrict-plus-operands': true,
        'strict-boolean-expressions': [
            false,
        ],
        'strict-type-predicates': true,
        'switch-default': true,
        'triple-equals': [
            true,
        ],
        // 'typeof-compare': true,
        'use-default-type-parameter': true,
        'use-isnan': true,
        'cyclomatic-complexity': [
            true,
            // tslint:disable-next-line: no-magic-numbers
            20,
        ],
        'deprecation': true,
        'eofline': true,
        'indent': [
            true,
            'spaces',
            // tslint:disable-next-line: no-magic-numbers
            2,
        ],
        'linebreak-style': [
            true,
            'LF',
        ],
        'max-classes-per-file': [
            true,
            1,
        ],
        'max-file-line-count': [
            true,
            // tslint:disable-next-line: no-magic-numbers
            500,
        ],
        'max-line-length': [
            false,
            {
                'limit': 120,
                'ignore-pattern': '^import |^export {(.*?)} |^ *//',
            },
        ],
        'no-default-export': false,
        'no-duplicate-imports': true,
        'no-mergeable-namespace': true,
        'object-literal-sort-keys': [
            false,
            'ignore-case',
            'match-declaration-order',
            'shorthand-first',
        ],
        'prefer-const': [
            true,
            {
                destructuring: 'any',
            },
        ],
        'prefer-readonly': [
            true,
        ],
        'trailing-comma': [
            true,
            {
                multiline: {
                    objects: 'always',
                    arrays: 'always',
                    functions: 'never',
                    typeLiterals: 'always',
                    imports: 'always',
                    exports: 'always',
                },
                singleline: {
                    objects: 'never',
                    arrays: 'never',
                    functions: 'never',
                    typeLiterals: 'never',
                    imports: 'never',
                    exports: 'never',
                },
                esSpecCompliant: true,
            },
        ],
        'align': [
            true,
            'arguments',
            'elements',
            'members',
            'parameters',
            'statements',
        ],
        'array-type': [
            true,
            'array',
        ],
        'arrow-parens': [
            true,
        ],
        'arrow-return-shorthand': [
            true,
            'multiline',
        ],
        'binary-expression-operand-order': false,
        'callable-types': true,
        'class-name': true,
        'comment-format': [
            true,
            // 'check-lowercase',
            {
                'ignore-words': [
                    'TODO',
                    'HACK',
                ],
            },
        ],
        'completed-docs': false,
        'encoding': true,
        'file-header': [
            false,
            'Copyright \\d{4}',
            'Copyright 2017',
        ],
        'import-spacing': true,
        'interface-name': [
            false,
            'always-prefix',
        ],
        'interface-over-type-literal': true,
        'jsdoc-format': [
            true,
            'check-multiline-start',
        ],
        'match-default-export-name': false,
        'newline-before-return': true,
        'newline-per-chained-call': false,
        'new-parens': true,
        'no-angle-bracket-type-assertion': true,
        'no-boolean-literal-compare': true,
        'no-consecutive-blank-lines': [
            true,
            // tslint:disable-next-line: no-magic-numbers
            2,
        ],
        'no-irregular-whitespace': true,
        'no-parameter-properties': false,
        'no-redundant-jsdoc': true,
        'no-reference-import': true,
        'no-trailing-whitespace': [
            true,
            'ignore-comments',
            'ignore-jsdoc',
            'ignore-template-strings',
            'ignore-blank-lines',
        ],
        'no-unnecessary-callback-wrapper': false,
        'no-unnecessary-initializer': true,
        'no-unnecessary-qualifier': true,
        'number-literal-format': true,
        'one-variable-per-declaration': [
            true,
            'ignore-for-loop',
        ],
        'object-literal-key-quotes': [
            true,
            'consistent-as-needed',
        ],
        'object-literal-shorthand': [
            true,
        ],
        'one-line': [
            true,
            'check-catch',
            'check-finally',
            'check-else',
            'check-open-brace',
            'check-whitespace',
        ],
        'ordered-imports': [
            true,
            {
                'import-sources-order': 'any',
                'grouped-imports': false,
                'named-imports-order': 'any',
                'module-source-path': 'full',
            },
        ],
        'prefer-function-over-method': [
            true,
        ],
        'prefer-method-signature': true,
        'prefer-switch': [
            true,
            {
                'min-cases': 2,
            },
        ],
        'prefer-template': [
            true,
        ],
        'quotemark': [
            true,
            'single',
            'jsx-double',
            'avoid-template',
            'avoid-escape',
        ],
        'return-undefined': true,
        'semicolon': [
            true,
            'always',
        ],
        'space-before-function-paren': [
            true,
            {
                anonymous: 'always',
                named: 'never',
                asyncArrow: 'always',
                method: 'never',
                constructor: 'never',
            },
        ],
        'space-within-parens': [
            true,
            0,
        ],
        'switch-final-break': [
            true,
            'always',
        ],
        'type-literal-delimiter': true,
        'variable-name': [
            true,
            'check-format',
            'allow-leading-underscore',
            // 'allow-trailing-underscore',
            'allow-pascal-case',
            // 'allow-snake-case',
            'ban-keywords',
        ],
        'whitespace': [
            true,
            'check-branch',
            'check-decl',
            'check-operator',
            'check-module',
            'check-separator',
            'check-rest-spread',
            'check-type',
            'check-typecast',
            'check-type-operator',
            'check-preblock',
        ],
        // tslint-consistent-codestyle
        // Declare parameters as const with JsDoc /** @const */
        'const-parameters': false,
        // Recommends to use an early exit instead of a long if block.
        'early-exit': [
            true,
            {
                'max-length': 2,
            },
        ],
        // Enforces where to consistently use curly braces where not strictly necessary.
        // 'ext-curly': 'braced-child',
        // Fine grained configuration to enfoce consistent naming for almost everything. E.g. variables, functions, classes, methods, parameters, enums, etc.
        // 'naming-convention': [
        //   true,
        //   // forbid leading and trailing underscores and enforce camelCase on EVERY name. will be overridden by subtypes if needed
        //   {type: 'default', format: 'camelCase', leadingUnderscore: 'forbid', trailingUnderscore: 'forbid'},
        //   // require all global constants to be camelCase or UPPER_CASE
        //   // all other variables and functions still need to be camelCase
        //   {type: 'variable', modifiers: ['global', 'const'], format: ['camelCase','UPPER_CASE']},
        //   // override the above format option for exported constants to allow only UPPER_CASE
        //   {type: 'variable', modifiers: ['export', 'const'], format: 'UPPER_CASE'},
        //   // require exported constant variables that are initialized with functions to be camelCase
        //   {type: 'functionVariable', modifiers: ['export', 'const'], format: 'camelCase'},
        //   // allow leading underscores for unused parameters, because `tsc --noUnusedParameters` will not flag underscore prefixed parameters
        //   // all other rules (trailingUnderscore: forbid, format: camelCase) still apply
        //   {type: 'parameter', modifiers: 'unused', leadingUnderscore: 'allow'},
        //   // require leading underscores for private properties and methods, all other rules still apply
        //   {type: 'member', modifiers: 'private', leadingUnderscore: 'require'},
        //   // same for protected
        //   {type: 'member', modifiers: 'protected', leadingUnderscore: 'require'},
        //   // exclicitly disable the format check only for method toJSON
        //   {type: 'method', filter: '^toJSON$', format: null},
        //   // enforce UPPER_CASE for all public static readonly(!) properties
        //   {type: 'property', modifiers: ['public', 'static', 'const'], format: 'UPPER_CASE'},
        //   // enforce PascalCase for classes, interfaces, enums, etc. Remember, there are still no underscores allowed.
        //   {type: 'type', format: 'PascalCase'},
        //   // abstract classes must have the prefix 'Abstract'. The following part of the name must be valid PascalCase
        //   {type: 'class', modifiers: 'abstract', prefix: 'Abstract'},
        //   // interface names must start with 'I'. The following part of the name must be valid PascalCase
        //   {type: 'interface', prefix: 'I'},
        //   // generic type parameters must start with 'T'
        //   // most of the time it will only be T, which is totally valid, because an empty string conforms to the PascalCase check
        //   // By convention T, U and V are used for generics. You could enforce that with 'regex': '^[TUV]$' and if you are care that much for performance, you could disable every other check by setting a falsy value
        //   {type: 'genericTypeParameter', prefix: 'T'},
        //   // enum members must be in PascalCase. Without this config, enumMember would inherit UPPER_CASE from public static const property
        //   {type: 'enumMember', format: 'PascalCase'}
        // ]
        // Prefer <Type>foo over foo as Type.
        'no-as-type-assertion': false,
        // Don't use get foo() { return this.foo; }. This is most likely a typo.
        'no-accessor-recursion': true,
        // Identifies nested if statements that can be combined into one.
        'no-collapsible-if': true,
        // Like no-else-return from eslint.
        'no-else-after-return': [
            true,
            'allow-else-if',
        ],
        // Just return; instead of return undefined;.
        'no-return-undefined': false,
        // Ban the use of this in static methods.
        'no-static-this': true,
        // Like no-else-after-return but better.
        'no-unnecessary-else': true,
        // Finds type annotations that can safely be removed.
        'no-unnecessary-type-annotation': false,
        // Find dead code and unused declarations.
        'no-unused': [
            true,
            'ignore-imports',
        ],
        // Checks if the returned variable is declared right before the return statement.
        'no-var-before-return': [
            true,
            'allow-destructuring',
        ],
        // Shorthand properties should precede regular properties.
        'object-shorthand-properties-first': true,
        // Require or disallow padding inside curly braces
        'object-curly-spacing': [
            true,
            'always',
            {
                arraysInObjects: true,
                objectsInObjects: true,
            },
        ],
        // Configure how and where to declare parameter properties.
        'parameter-properties': [
            false,
        ],
        // Prefer const enum where possible.
        'prefer-const-enum': true,
        // Prefer a while loop instead of a for loop without initializer and incrementer.
        'prefer-while': true,
        //
        //
        // tslint-microsoft-contrib
        //
        //
        // Avoid Chai assertions that invoke indexOf and compare for a -1 result. It is better to use the chai .contain() assertion API instead because the failure message will be more clearer if the test fails.
        'chai-prefer-contains-to-index-of': true,
        // Avoid Chai assertions that result in vague errors. For example, asserting expect(something).to.be.true will result in the failure message "Expected true received false". This is a vague error message that does not reveal the underlying problem. It is especially vague in TypeScript because stack trace line numbers often do not match the source code. A better pattern to follow is the xUnit Patterns Assertion Message pattern. The previous code sample could be better written as expect(something).to.equal(true, 'expected something to have occurred');
        'chai-vague-errors': true,
        // The name of the exported module must match the filename of the source file. This is case-sensitive but ignores file extension. Since version 1.0, this rule takes a list of regular expressions as a parameter. Any export name matching that regular expression will be ignored. For example, to allow an exported name like myChartOptions, then configure the rule like this: "export-name": [true, "myChartOptionsg"]
        'export-name': false,
        // Applies a naming convention to function names and method names. You can configure the naming convention by passing parameters. Please note, the private-method-regex does take precedence over the static-method-regex, so a private static method must match the private-method-regex. The default values are:
        // This rule has some overlap with the tslint variable-name rule; however, the rule here is more configurable.]
        'function-name': [
            false, {
            //     "method-regex": "^[a-z][\w\d]+$",
            //     "private-method-regex": "^[a-z][\w\d]+$",
            //     "protected-method-regex": "^[a-z][\w\d]+$",
            //     "static-method-regex": "^[A-Z_\d]+$",
            //     "function-regex": "^[a-z][\w\d]+$"
            },
        ],
        // The name of the imported module must match the name of the thing being imported. For example, it is valid to name imported modules the same as the module name: import Service = require('x/y/z/Service') and import Service from 'x/y/z/Service'. But it is invalid to change the name being imported, such as: import MyCoolService = require('x/y/z/Service') and import MyCoolService from 'x/y/z/Service'. Since version 2.0.9 it is possible to configure this rule with a list of exceptions. For example, to allow underscore to be imported as _, add this configuration: 'import-name': [ true, { 'underscore': '_' }]
        'import-name': false,
        // Do not use insecure sources for random bytes. Use a secure random number generator instead. Bans all uses of Math.random and crypto.pseudoRandomBytes. Better alternatives are crypto.randomBytes and window.crypto.getRandomValues.
        // References:
        // * CWE 330
        // * MDN Math.random
        // * Node.js crypto.randomBytes()
        // * window.crypto.getRandomValues()
        // 2.0.11
        'insecure-random': true,
        // When a JQuery Deferred instance is created, then either reject() or resolve() must be called on it within all code branches in the scope. For more examples see the feature request.
        'jquery-deferred-must-complete': true,
        // Avoid long functions. The line count of a function body must not exceed the value configured within this rule's options.
        // You can setup a general max function body length applied for every function/method/arrow function e.g. [true, 30] or set different maximum length for every type e.g. [true, { "func-body-length": 10 , "func-expression-body-length": 10 , "arrow-body-length": 5, "method-body-length": 15, "ctor-body-length": 5 }]. To specify a function name whose parameters you can ignore for this rule, pass a regular expression as a string(this can be useful for Mocha users to ignore the describe() function). Since version 2.0.9, you can also ignore single- and multi-line comments from the total function length, eg. [true, { "ignore-comments": true }]
        'max-func-body-length': [
            false,
            {
                'func-body-length': 10,
                'func-expression-body-length': 10,
                'arrow-body-length': 5,
                'method-body-length': 15,
                'ctor-body-length': 5,
                'ignore-comments': true,
            },
        ],
        // All files must have a top level JSDoc comment. A JSDoc comment starts with /** (not one more or one less asterisk) and a JSDoc at the 'top-level' appears without leading spaces. Trailing spaces are acceptable but not recommended.
        'missing-jsdoc': false,
        // Do not invoke Mocha's describe.only, it.only or context.only functions. These functions are useful ways to run a single unit test or a single test case during your build, but please be careful to not push these methods calls to your version control repositiory because it will turn off any of the other tests.
        'mocha-avoid-only': true,
        // All test logic in a Mocha test case should be within Mocha lifecycle method and not defined statically to execute when the module loads. Put all assignments and initialization statements in a before(), beforeEach(), beforeAll(), after(), afterEach(), afterAll(), or it() function. Code executed outside of these lifecycle methods can throw exceptions before the test runner is initialized and can result in errors or even test runner crashes. This rule can be configured with a regex to ignore certain initializations. For example, to ignore any calls to RestDataFactory configure the rule with: [true, { ignore: '^RestDataFactory\\..*' }]
        'mocha-no-side-effect-code': false,
        // A function declares a MochaDone parameter but only resolves it synchronously in the main function. The MochaDone parameter can be safely removed from the parameter list.
        'mocha-unneeded-done': true,
        // Avoid using model.get('x') and model.set('x', value) Backbone accessors outside of the owning model. This breaks type safety and you should define getters and setters for your attributes instead.
        'no-backbone-get-set-outside-model': false,
        // Do not use banned terms: caller, callee, eval, arguments. These terms refer to functions or properties that should not be used, so it is best practice to simply avoid them.
        'no-banned-terms': true,
        // Do not use constant expressions in conditions. Similar to the ESLint no-constant-condition rule. Since version 2.0.14, this rule accepts a parameter called checkLoops which defaults to true. If set to false then loops are not checked for conditionals. For example, disable loop checking with [ true, { 'checkLoops': false } ]
        'no-constant-condition': [
            true,
            {
                checkLoops: true,
            },
        ],
        // Do not use control characters in regular expressions . Similar to the ESLint no-control-regex rule
        'no-control-regex': true,
        // Do not use cookies
        'no-cookies': true,
        // Do not delete expressions. Only properties should be deleted
        'no-delete-expression': true,
        // Do not disable auto-sanitization of HTML because this opens up your page to an XSS attack. Specifically, do not use the execUnsafeLocalFunction or setInnerHTMLUnsafe functions.
        'no-disable-auto-sanitization': true,
        // Do not write to document.domain. Scripts setting document.domain to any value should be validated to ensure that the value is on a list of allowed sites. Also, if your site deals with PII in any way then document.domain must not be set to a top-level domain (for example, live.com) but only to an appropriate subdomain (for example, billing.live.com). If you are absolutely sure that you want to set document.domain then add a tslint suppression comment for the line. For more information see the Phase 4 Verification page of the Microsoft SDL
        'no-document-domain': true,
        // Do not use document.write
        'no-document-write': true,
        // Avoid an empty line after an opening brace.
        'no-empty-line-after-opening-brace': true,
        // Do not use the execScript functions
        'no-exec-script': true,
        // Avoid use of for-in statements. They can be replaced by Object.keys
        'no-for-in': true,
        // Prevents using the built-in Function constructor.
        'function-constructor': true,
        // Do not use function expressions; use arrow functions (lambdas) instead. In general, lambdas are simpler to use and avoid the confusion about what the 'this' references points to. Function expressions that contain a 'this' reference are allowed and will not create a failure.
        'no-function-expression': true,
        // Do not use strings that start with 'http:'. URL strings should start with 'https:'. Http strings can be a security problem and indicator that your software may suffer from cookie-stealing attacks. Since version 1.0, this rule takes a list of regular expressions as a parameter. Any string matching that regular expression will be ignored. For example, to allow http connections to example.com and examples.com, configure your rule like this: "no-http-string": [true, "http://www.example.com/?.*", "http://www.examples.com/?.*"\]
        'no-http-string': false,
        // Enforces using explicit += 1 or -= 1 operators.
        'increment-decrement': true,
        // Do not write values to innerHTML, outerHTML, or set HTML using the JQuery html() function. Writing values to innerHTML can expose your website to XSS injection attacks. All strings must be escaped before being rendered to the page.
        'no-inner-html': true,
        // Do not use invalid regular expression strings in the RegExp constructor. Similar to the ESLint no-invalid-regexp rule
        'no-invalid-regexp': true,
        // Do not create HTML elements using JQuery and string concatenation. It is error prone and can hide subtle defects. Instead use the JQuery element API.
        'no-jquery-raw-elements': true,
        // Do not declare multiline strings
        'no-multiline-string': false,
        // Do not use octal literals or escaped octal sequences
        'no-octal-literal': true,
        // Do not use multiple spaces in a regular expression literal. Similar to the ESLint no-regex-spaces rule
        'no-regex-spaces': true,
        // Do not use relative paths when importing external modules or ES6 import declarations. The advantages of removing all relative paths from imports is that 1) the import name will be consistent across all files and subdirectories so searching for usages is much easier. 2) Moving source files to different folders will not require you to edit your import statements. 3) It will be possible to copy and paste import lines between files regardless of the file location. And 4) version control diffs will be simplified by having overall fewer edits to the import lines.
        'no-relative-imports': false,
        // Avoid single line block comments and use single line comments instead. Block comments do not nest properly and have no advantages over normal single-line comments
        'no-single-line-block-comment': false,
        // Do not use the version of setImmediate that accepts code as a string argument. However, it is acceptable to use the version of setImmediate where a direct reference to a function is provided as the callback argument
        'no-string-based-set-immediate': true,
        // Do not use the version of setInterval that accepts code as a string argument. However, it is acceptable to use the version of setInterval where a direct reference to a function is provided as the callback argument
        'no-string-based-set-interval': true,
        // Do not use the version of setTimeout that accepts code as a string argument. However, it is acceptable to use the version of setTimeout where a direct reference to a function is provided as the callback argument
        'no-string-based-set-timeout': true,
        // Do not use suspicious comments, such as BUG, HACK, FIXME, LATER, LATER2, TODO. We recommend that you run this rule before each release as a quality checkpoint. Reference: CWE-546 Suspicious Comment
        'no-suspicious-comment': false,
        // Do not use the idiom typeof x === 'undefined'. You can safely use the simpler x === undefined or perhaps x == null if you want to check for either null or undefined.
        'no-typeof-undefined': true,
        // Ensures that double quoted strings are passed to a localize call to provide proper strings for different locales. The rule can be configured using an object literal as document in the feature request
        'no-unexternalized-strings': false,
        // Do not bind 'this' as the context for a function literal or lambda expression. If you bind 'this' as the context to a function literal, then you should just use a lambda without the bind. If you bind 'this' as the context to a lambda, then you can remove the bind call because 'this' is already the context for lambdas. Works for Underscore methods as well.
        'unnecessary-bind': true,
        // Do not unnecessarily initialize the fields of a class to values they already have. For example, there is no need to explicitly set a field to undefined in the field's initialization or in the class' constructor. Also, if a field is initialized to a constant value (null, a string, a boolean, or some number) then there is no need to reassign the field to this value within the class constructor.
        'no-unnecessary-field-initialization': true,
        // Do not declare a variable only to return it from the function on the next line. It is always less code to simply return the expression that initializes the variable.
        'no-unnecessary-local-variable': true,
        // Do not write a method that only calls super() on the parent method with the same arguments. You can safely remove methods like this and Javascript will correctly dispatch the method to the parent object.
        'no-unnecessary-override': true,
        // Remove unnecessary semicolons
        'no-unnecessary-semicolons': true,
        // Avoid writing browser-specific code for unsupported browser versions. Browser versions are specified in the rule configuration options, eg: [true, [ "IE 11", "Firefox > 40", "Chrome >= 45" ] ]. Browser-specific blocks of code can then be designated with a single-line comment, like so: // Browser specific: IE 10, or with a jsdoc like this: @browserspecific chrome 40.
        'no-unsupported-browser-code': false,
        // Avoid keeping files around that only contain commented out code, are completely empty, or only contain whitespace characters
        'no-useless-files': true,
        // Do not use with statements. Assign the item to a new variable instead
        'no-with-statement': true,
        // Detect require() function calls for something that is not a string literal. For security reasons, it is best to only require() string literals. Otherwise, it is perhaps possible for an attacker to somehow change the value and download arbitrary Javascript into your page.
        'non-literal-require': true,
        // Avoid timing attacks by not making direct string comparisons to sensitive data. Do not compare against variables named password, secret, api, apiKey, token, auth, pass, or hash. For more info see Using Node.js Event Loop for Timing Attacks
        'possible-timing-attack': true,
        // Use array literal syntax when declaring or instantiating array types. For example, prefer the Javascript form of string[] to the TypeScript form Array. Prefer '[]' to 'new Array()'. Prefer '[4, 5]' to 'new Array(4, 5)'. Prefer '[undefined, undefined]' to 'new Array(4)'. Since 2.0.10, this rule can be configured to allow Array type parameters. To ignore type parameters, configure the rule with the values: [ true, { 'allow-type-parameters': true } ]
        // 'This rule has some overlap with the TSLint array-type rule, however, the version here catches more instances.': true,
        'prefer-array-literal': true,
        // Prefer the tradition type casts instead of the new 'as-cast' syntax. For example, prefer <string>myVariable instead of myVariable as string. Rule ignores any file ending in .tsx. If you prefer the opposite and want to see the as type casts, then enable the tslint rule named 'no-angle-bracket-type-assertion'
        'prefer-type-cast': false,
        // When a Promise instance is created, then either the reject() or resolve() parameter must be called on it within all code branches in the scope. For more examples see the feature request.
        // This rule has some overlap with the tslint no-floating-promises rule, but they are substantially different.
        'promise-must-complete': true,
        // For accessibility of your website, anchor element link text should be at least 4 characters long. Links with the same HREF should have the same link text. Links that point to different HREFs should have different link text. Links with images and text content, the alt attribute should be unique to the text content or empty. An an anchor element's href prop value must not be just #.
        // References:
        // WCAG Rule 38: Link text should be as least four 4 characters long
        // WCAG Rule 39: Links with the same HREF should have the same link text
        // WCAG Rule 41: Links that point to different HREFs should have different link text
        // WCAG Rule 43: Links with images and text content, the alt attribute should be unique to the text content or empty
        // 2.0.11
        'react-a11y-anchors': true,
        // For accessibility of your website, enforce that elements that do not support ARIA roles, states, and properties do not have those attributes.
        'react-a11y-aria-unsupported-elements': true,
        // For accessibility of your website, Elements with event handlers must have explicit role or implicit role.
        // References:
        // WCAG Rule 94
        // Using the button role
        'react-a11y-event-has-role': true,
        // For accessibility of your website, enforce that inputs element with type="image" must have non-empty alt attribute.
        'react-a11y-image-button-has-alt': true,
        // Enforce that an img element contains the alt attribute or role='presentation' for a decorative image. All images must have alt text to convey their purpose and meaning to screen reader users. Besides, the alt attribute specifies an alternate text for an image, if the image cannot be displayed. This rule accepts as a parameter a string array for tag names other than img to also check. For example, if you use a custom tag named 'Image' then configure the rule with: [true, ['Image']]
        // References:
        // Web Content Accessibility Guidelines 1.0
        // ARIA Presentation Role
        // WCAG Rule 31: If an image has an alt or title attribute, it should not have a presentation role
        'react-a11y-img-has-alt': true,
        // For accessibility of your website, HTML elements must have a lang attribute and the attribute must be a valid language code.
        // References:
        // * H58: Using language attributes to identify changes in the human language
        // * lang attribute must have a valid value
        // List of ISO 639-1 codes
        'react-a11y-lang': true,
        // For accessibility of your website, HTML meta elements must not have http-equiv="refresh".
        'react-a11y-meta': true,
        // For accessibility of your website, enforce all aria-* attributes are valid. Elements cannot use an invalid aria-* attribute. This rule will fail if it finds an aria-* attribute that is not listed in WAI-ARIA states and properties.
        'react-a11y-props': true,
        // For accessibility of your website, enforce the type of aria state and property values are correct.
        'react-a11y-proptypes': true,
        // For accessibility of your website, elements with aria roles must have all required attributes according to the role.
        // References:
        // ARIA Definition of Roles
        // WCAG Rule 90: Required properties and states should be defined
        // WCAG Rule 91: Required properties and states must not be empty
        // 2.0.11
        'react-a11y-role-has-required-aria-props': true,
        // For accessibility of your website, enforce that elements with explicit or implicit roles defined contain only aria-* properties supported by that role. Many aria attributes (states and properties) can only be used on elements with particular roles. Some elements have implicit roles, such as <a href='hrefValue' />, which will be resolved to role='link'. A reference for the implicit roles can be found at Default Implicit ARIA Semantics.
        // References:
        // * ARIA attributes can only be used with certain roles
        // * Check aria properties and states for valid roles and properties
        // * Check that 'ARIA-' attributes are valid properties and states
        'react-a11y-role-supports-aria-props': true,
        // For accessibility of your website, elements with aria roles must use a valid, non-abstract aria role. A reference to role defintions can be found at WAI-ARIA roles. References:
        // * WCAG Rule 92: Role value must be valid
        'react-a11y-role': true,
        // For accessibility of your website, enforce tabindex value is not greater than zero. Avoid positive tabindex attribute values to synchronize the flow of the page with keyboard tab order.
        // References:
        // WCAG 2.4.3 - Focus Order
        // Audit Rules - tabindex-usage
        // Avoid positive integer values for tabIndex
        'react-a11y-tabindex-no-positive': true,
        // For accessibility of your website, HTML title elements must not be empty, must be more than one word, and must not be more than 60 characters long.
        // References:
        // * WCAG 2.0 - Requirement 2.4.2 Page Titled (Level A)
        // * OAA-Accessibility Rule 13: Title element should not be empty
        // * OAA-Accessibility Rule 24: Title content should be concise
        // * OAA-Accessibility Rule 25: Title text must contain more than one word
        // 2.0.11
        'react-a11y-titles': true,
        // For security reasons, anchor tags with target="_blank" should also include rel="noopener noreferrer". In order to restrict the behavior window.opener access, the original page needs to add a rel="noopener" attribute to any link that has target="_blank". However, Firefox does not support that tag, so you should actually use rel="noopener noreferrer" for full coverage. For more info see: The target="_blank" vulnerability by example
        'react-anchor-blank-noopener': true,
        // React iframes must specify a sandbox attribute. If specified as an empty string, this attribute enables extra restrictions on the content that can appear in the inline frame. The value of the attribute can either be an empty string (all the restrictions are applied), or a space-separated list of tokens that lift particular restrictions. You many not use both allow-scripts and allow-same-origin at the same time, as that allows the embedded document to programmatically remove the sandbox attribute in some scenarios.
        'react-iframe-missing-sandbox': true,
        // Do not use React's dangerouslySetInnerHTML API. This rule finds usages of the dangerouslySetInnerHTML API (but not any JSX references). For more info see the react-no-dangerous-html Rule wiki page.
        'react-no-dangerous-html': true,
        // Several errors can occur when using React and React.Component subclasses. When using React components you must be careful to correctly bind the 'this' reference on any methods that you pass off to child components as callbacks. For example, it is common to define a private method called 'onClick' and then specify onClick={this.onClick} as a JSX attribute. If you do this then the 'this' reference will be undefined when your private method is invoked. The React documentation suggests that you bind the 'this' reference on all of your methods within the constructor: this.onClick = this.onClick.bind(this);. This rule will create a violation if 1) a method reference is passed to a JSX attribute without being bound in the constructor. And 2) a method is bound multiple times in the constructor. Another issue that can occur is binding the 'this' reference to a function within the render() method. For example, many people will create an anonymous lambda within the JSX attribute to avoid the 'this' binding issue: onClick={() => { this.onClick(); }}. The problem with this is that a new instance of an anonymous function is created every time render() is invoked. When React compares virutal DOM properties within shouldComponentUpdate() then the onClick property will look like a new property and force a re-render. You should avoid this pattern because creating function instances within render methods breaks any logic within shouldComponentUpdate() methods. This rule creates violations if 1) an anonymous function is passed as a JSX attribute. And 2) if a function instantiated in local scope is passed as a JSX attribute. This rule can be configured via the "allow-anonymous-listeners" parameter. If you want to suppress violations for the anonymous listener scenarios then configure that rule like this: "react-this-binding-issue": [ true, { 'allow-anonymous-listeners': true } ]
        'react-this-binding-issue': true,
        // Consistently use spaces around the brace characters of JSX attributes. You can either allow or ban spaces between the braces and the values they enclose.
        // One of the two following options are required:
        // * "always" enforces a space inside of curly braces (default)
        // * "never" disallows spaces inside of curly braces
        // By default, braces spanning multiple lines are not allowed with either setting. If you want to allow them you can specify an additional allowMultiline property with the value false.
        // Examples:
        // * "react-tsx-curly-spacing": [true, "always"]
        // * "react-tsx-curly-spacing": [true, "never"]
        // * "react-tsx-curly-spacing": [true, "never", {"allowMultiline": false}]
        // References
        // * eslint-plugin-react jsx-curly-spacing rule
        // * tslint-react jsx-curly-spacing rule
        'react-tsx-curly-spacing': [
            true,
            'never',
            {
                allowMultiline: true,
            },
        ],
        // Remove unneeded properties defined in React Props and State interfaces. Any interface named Props or State is defined as a React interface. All fields in these interfaces must be referenced. This rule can be configured with regexes to match custom Props and State interface names.
        // Example for including all interfaces ending with Props or State:
        // [ true, { 'props-interface-regex': 'Props$', 'state-interface-regex': 'State$' } ]
        'react-unused-props-and-state': [
            false,
            {
                'props-interface-regex': 'Props$',
                'state-interface-regex': 'State$',
            },
        ],
        // Enforce a consistent usage of the _ functions. By default, invoking underscore functions should begin with wrapping a variable in an underscore instance: _(list).map(...). An alternative is to prefer using the static methods on the _ variable: _.map(list, ...). The rule accepts single parameter called 'style' which can be the value 'static' or 'instance': [true, { "style": "static" }]
        'underscore-consistent-invocation': [
            false,
            {
                style: 'static',
            },
        ],
        // Do not reference the arguments object by numerical index; instead, use a named parameter. This rule is similar to JSLint's Use a named parameter rule.
        'use-named-parameter': true,
    },
};

